# Kendo UI grid task

## Define a Kendo UI Grid that has the following features:

* Sits on a page with h1 element

* The heading and the Grid together should take the entire height of the page. Both should be visible at all times

* Grid should have two locked columns

* Grid should have paging

* One of the columns in the Grid should be sorted initially in descending order  



## Getting Started <a name="gettingStarted"></a>
This project is based on the official jQuery bundle for Kendo UI

To preview the task described above navigate to `apptemplates/grid-and-menu` and open `index.html`


## Author
 - [Nikolay Penev](https://gitlab.com/NikoPenev)




